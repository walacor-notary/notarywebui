import React from 'react';
import logo from './logo.svg';
import './App.css';
import { BrowserRouter as Router } from "react-router-dom";
import AppRoute from './app.route';
import { connect } from 'react-redux';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
const App: React.FC<AppState> = (props: AppState) => {

	return (
		<div>
			<Router>
				{AppRoute}
				<ToastContainer />
			</Router>
		</div>

	);
}
const mapStateToProps = (state: any) => {
	return {
		isLoaderShow: state.appLoaderReducer.isLoaderShow
	}
}

export default connect(mapStateToProps)(App);

interface AppState {
	isLoaderShow: boolean
}

import { Route, Redirect, Switch } from 'react-router-dom';
 import  React from 'react';
 import PagesComponent from './app/modules/pages/pages';
 const AppRoute = (
    <Route>
        <Switch>
            <Route path="/page" component={PagesComponent} />
            <Redirect from='*' to='/page' />
        </Switch>
    </Route>
);

export default AppRoute;
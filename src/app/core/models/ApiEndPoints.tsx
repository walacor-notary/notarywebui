
export class ApiEndPoints {
    public static USER_LOGIN: APIDef = { 'method': 'POST', api: () => `/api/auth/login` };

}

export interface APIDef {
    method: string;
    api: any;
}

export interface APIInput {
    email?: string;
}





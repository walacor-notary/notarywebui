import React from 'react';
import { DashboardRounded, SupervisorAccount, Work, Equalizer } from '@material-ui/icons';
const SidebarMenu = [
    {
        name: 'Dashboard',
        icon: <DashboardRounded color="inherit" />,
        url: '/page/user-dashboard',
        tooltip: 'Dashboard',
        placement: 'right'

    },
    {
        name: 'Verify & Share',
        icon: <Equalizer color="inherit" />,
        url: '/page/data-dashboard',
        tooltip: 'Verify & Share',
        placement: 'right'

    }
];

export default SidebarMenu;
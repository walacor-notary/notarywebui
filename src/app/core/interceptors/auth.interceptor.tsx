import axios from 'axios';
import { CookieStorage } from 'cookie-storage';
const cookies = new CookieStorage();

export class AuthService {
    constructor() { }
    public setAuthorizationToken = (token: any) => {
        if (token) {
            axios.defaults.headers.common['Authorization'] = `${token}`;
            
        } else {
            delete axios.defaults.headers.common['Authorization'];
        }
    }

    public setToken = (resptoken: any) => {
        const date = new Date();
        const days = 0.5;
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        cookies.setItem('access_token', resptoken.data.data.api_token, { expires: date });
        this.setAuthorizationToken(resptoken.data.data.api_token);
        return resptoken;

    }
    public logout() {
        cookies.removeItem('access_token');
        cookies.clear();
    }
}
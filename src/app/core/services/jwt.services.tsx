import jwtDecode from 'jwt-decode';
import { CookieStorage } from 'cookie-storage';
import * as _ from 'lodash';
import { string } from 'prop-types';
const cookies = new CookieStorage();
export class JwtServices {
    constructor() {
 }
    public  getJWTToken() : any {
        return cookies.getItem('access_token');
    }

    public  getDecodedJWTToken(): any {
        try {
           return jwtDecode(this.getJWTToken());
        } catch {
            console.log('not the valid jwt token');
        }
    }

    public  getUserRoles() {
        return !_.isEmpty(this.getDecodedJWTToken()) ? this.getDecodedJWTToken().role : [];
    }

    public  isSupperAdmin() {
        var data=this.getUserRoles();        
        return data==="System Admin";
    }
   
    public isLogin()
    {
        return this.getJWTToken() !=null;
    }
}
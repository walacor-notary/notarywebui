import { ToastContainer, toast, Slide, Zoom, Flip, Bounce } from 'react-toastify';



class Toaster {

    public successToaster(toasterId: any, txt: string) {
        if (!toast.isActive(toasterId)) {
            toast.success(txt, {
                position: toast.POSITION.TOP_RIGHT,
                autoClose: 2000,
                transition: Slide,
                toastId: toasterId,
            });
        }
    }
    public infoToaster(txt: string) {
        toast.info(txt, {
            position: toast.POSITION.TOP_RIGHT,
            autoClose: 3000,
            transition: Slide
        });
    }
    public errorToaster(txt: string) {
        toast.error(txt, {
            position: toast.POSITION.TOP_RIGHT,
            autoClose: 2000,
            transition: Slide
        });
    }

    public warnToaster(txt: string) {
        toast.warn(txt, {
            position: toast.POSITION.TOP_RIGHT,
            autoClose: 2000,
            transition: Slide
        });

    }
    public basicToaster(txt: string) {
        toast(txt, {
            position: toast.POSITION.TOP_RIGHT,
            autoClose: 2000,
            transition: Slide
        });
    }

    public errorToasterWithId(toasterId: any, txt: string) {
        if (!toast.isActive(toasterId)) {
            toast.error(txt, {
                position: toast.POSITION.TOP_RIGHT,
                autoClose: 2000,
            });
        }
    }

    public errorToasterWith30S(txt: string) {
        toast.error(txt, {
            position: toast.POSITION.TOP_CENTER,
            autoClose: 3000,
        });
    }
}

export default new Toaster();
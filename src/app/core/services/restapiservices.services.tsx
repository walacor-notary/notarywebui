import axios from 'axios';
import { APIDef, APIInput } from '../models/ApiEndPoints';
import { AuthService } from '../interceptors/auth.interceptor';
import { JwtServices } from './jwt.services';
export class RestApiService {
    public authSerivce = new AuthService();
    public jwtServices = new JwtServices();
    public blackListAPI = ['oauth/token', 'api/otps/registerUser', 'api/users', 'api/otps/resetPassword',
        'api/reset-password', 'api/specialities']
    public invoke<T>(def: APIDef, apiInput: APIInput = {}, data?: T, queryMap?: any, headerObject?: any) {
        if (this.blackListAPI.indexOf(def.api) === -1) {
            this.authSerivce.setAuthorizationToken(this.jwtServices.getJWTToken());
        } else {
            delete axios.defaults.headers.common['Authorization'];
        }
        return this.invokeAPI(def.api(apiInput), def.method, data, queryMap, headerObject);
    }

    private invokeAPI<T>(api: string, method: string, body?: T, queryMap?: any, headerObject?: any) {
        const headers: any = {
            Accept: 'application/json',
            'Content-Type': 'application/json',

        }
        if (headerObject) {
            if (headerObject.ETId)
                headers.ETId = headerObject.ETId;
            if (headerObject.DV)
                headers.DV = headerObject.DV;
        }

        const httpOptions = { headers: headers, params: queryMap, observe: 'body' };
        switch (method) {
            case 'POST':
                return this.post<T>(api, body, httpOptions);
            case 'GET':
                return this.get<T>(api, httpOptions);
            case 'PUT':
                return this.put<T>(api, body, httpOptions);
            case 'DELETE':
                return this.delete<T>(api, httpOptions);
            default:
                break;
        }
    }

    private post<T>(apiUrl: any, body: any, httpOptions: any) {
        return axios({
            method: 'post',
            url: apiUrl,
            headers: httpOptions.headers,
            params: httpOptions.params,
            data: body
        })
    }

    private get<T>(apiUrl: any, httpOptions: any) {
        return axios({
            method: 'get',
            url: apiUrl,
            params: httpOptions.params,
            headers: httpOptions.headers
        })
    }

    private put<T>(apiUrl: any, body: any, httpOptions: any) {
        return axios({
            method: 'put',
            url: apiUrl,
            headers: httpOptions.headers,
            params: httpOptions.params,
            data: body
        })
    }

    private delete<T>(apiUrl: any, httpOptions: any) {
        return axios({
            method: 'delete',
            url: apiUrl,
            headers: httpOptions.headers,
            params: httpOptions.params
        })
    }
}

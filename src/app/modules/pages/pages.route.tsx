import { Route, Switch, Redirect } from 'react-router-dom';
import React, { Component } from 'react';
import { JwtServices } from '../../core/services/jwt.services';
const jwtServices = new JwtServices();
const PrivateRoute = ({ component: Component, ...rest }: any) => (
    <Route {...rest} render={(props) => (
        <Route {...rest} render={(props) => (
            jwtServices.isLogin() === true
                ? <Component {...props} />
                : <Redirect to='/welcome' />
        )} />
    )} />
)



const PageRoute = (props: any) => {
    return (
        <Switch>
        </Switch>
    )
}

export default PageRoute;
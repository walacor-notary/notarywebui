
import React, { Component } from 'react';
import './pages.scss'
import PageRoute from './pages.route';
import Headercomponent from '../../shared/components/header/header.component';
import SideBarComponent from '../../shared/components/sideBar/sideBar.component';
import { connect } from 'react-redux';
import { Loader } from '../../shared/components/loader/loader';

class PagesComponent extends Component<any, any> {

    public state = {
        openAdminMenu: false,
        openMenu: true
    };
    constructor(props: any) {
        super(props);
        this.isOpenMenu = this.isOpenMenu.bind(this);
        this.detectMobileView = this.detectMobileView.bind(this);
    }

    componentDidMount() {
        this.detectMobileView();
    }

    public isOpenMenu(value: any) {
        this.setState({ openMenu: value })
    }

    public detectMobileView() {
        if (window.innerWidth <= 767) {
            this.setState({ openMenu: false })
        } else {
            this.setState({ openMenu: true })
        }
    }

    render() {
        return (
            <div className="main-view">
                <Headercomponent openMenu={this.state.openMenu} onClick={this.isOpenMenu} />
                <div className="main-content">
                    <div className={`sidebar-cover ${!this.state.openMenu ? 'hide-sidebar' : ''}`}>
                        <SideBarComponent openMenu={this.state.openMenu} />
                    </div>
                    <div className={`dashboard-cover ${!this.state.openMenu ? 'hide-sidebar' : ''}`}>
                        {PageRoute(this.props)}
                    </div>
                </div>
                {this.props.isLoaderShow && <Loader />}
            </div>
        );
    }
}

const mapStateToProps = (state: any) => {
    return {
        isLoaderShow: state.appLoaderReducer.isLoaderShow,
        isShowSideBar: state.appSidebarReducer
    }
}

export default connect(mapStateToProps)(PagesComponent);

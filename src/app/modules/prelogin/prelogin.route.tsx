import {  Route, Switch } from 'react-router-dom';
import * as React from 'react';
import LoginComponent from './login/login.component';


const PreloginRoute = (props: any) => (
    <Switch>
        <Route exact path={`${props.match.path}`} component={LoginComponent} />
        <Route exact path={`${props.match.path}/login`} component={LoginComponent} />
    </Switch>
);

export default PreloginRoute;

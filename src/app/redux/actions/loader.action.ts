import { TOGGLE_LOADER } from '../types/loader.types';

export function toggleLoader(payload: boolean) {   
    return {
        type: TOGGLE_LOADER,
        payload
    }
}

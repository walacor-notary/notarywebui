import { combineReducers } from 'redux';
import appLoaderReducer from './loader.reducer';


export default combineReducers({
    appLoaderReducer: appLoaderReducer,

});
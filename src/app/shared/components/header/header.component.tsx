import React, { Component } from 'react';
import { Link, NavLink } from "react-router-dom";
import './header.component.scss';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MenuIcon from '@material-ui/icons/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import { withRouter } from 'react-router'
import { AuthService } from '../../../core/interceptors/auth.interceptor';
import { CookieStorage } from 'cookie-storage';
const cookies = new CookieStorage();

class Headercomponent extends Component<any, any> {


    constructor(props: any) {
        super(props);
        this.state = {
            anchorEl: null,
            existingToken: null
        }
        this.handleClick = this.handleClick.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.onLogout = this.onLogout.bind(this);
    }

    public authService = new AuthService();
    public handleClick(event: React.MouseEvent<HTMLButtonElement>) {
        this.setState({ anchorEl: event.currentTarget });
    }

    public handleClose() {
        this.setState({ anchorEl: null });
    }

    public handleSidebar() {
        this.setState({ anchorEl: null });
    }

    onLogout(path: any) {
        console.log('props', this.props, path);
        this.props.history.push(path);
        this.authService.logout();
    }

    componentDidMount() {
        this.setState({ existingToken: cookies.getItem('access_token') });
    }

    render() {
        return (
            <div className="page-header">
                <AppBar position="static">
                    <Toolbar>
                        {this.state.existingToken &&
                            <IconButton edge="start"
                                className="menuButton"
                                color="inherit" aria-label="menu"
                                onClick={() => this.props.onClick(!this.props.openMenu)}>
                                <MenuIcon />
                            </IconButton>}
                        <span className="logo">
                            <img src={require('../../../../assets/images/logo.png')} alt="Notary" title={"Notary"} />
                        </span>
                        <Typography variant="h6" className="title">
                            Notary
                        </Typography>
                        {this.state.existingToken && <IconButton
                            aria-label="account of current user"
                            aria-controls="menu-appbar"
                            aria-haspopup="true"
                            color="inherit"
                            onClick={this.handleClick}
                        >
                            <AccountCircle />
                        </IconButton>}
                        <Menu
                            id="menu-appbar"
                            anchorEl={this.state.anchorEl}
                            anchorOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                            keepMounted
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                            open={Boolean(this.state.anchorEl)}
                            onClose={this.handleClose}
                        >

                            <MenuItem onClick={() => this.onLogout('/login')}>Logout</MenuItem>
                        </Menu>
                    </Toolbar>
                </AppBar>
            </div>
        );
    }
}

export default withRouter(Headercomponent);

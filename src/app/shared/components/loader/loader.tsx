import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import './loader.scss';

export function Loader() {
    return (
        <div className="loader-wrapper">
            <div id="loader">
                <CircularProgress size={70} thickness={6} color="primary" />
            </div>
        </div>
    );
}


export function SmallLoader() {
    return (
        <div className="small-loader">
            <CircularProgress className="loader-icon" color="primary" />
        </div>
    );
}



import React from 'react';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { NavLink } from "react-router-dom";
import SidebarMenu from '../../../core/models/SidebarMenu.list';
import './sideBar.component.scss';
import { Tooltip } from '@material-ui/core';


const SideBarComponent = (props: any) => {
    return (
        <Drawer
            variant="permanent"
            className={`drawer ${!props.openMenu ? 'hide-sidebar' : ''}`}
        >
            <List>
                {SidebarMenu.map((item: any, index: any) => (item.type && item.type === 'DROPDOWN') ? item.menu(props) :

                    <li className={`menu-link`} key={"s_nll" + index} >

                        <NavLink activeClassName="active" className="link" to={item.url} key={"s_nl" + index}>
                            <Tooltip title={item.tooltip} placement={item.placement}>
                                <ListItemIcon key={"s_nli" + index} >{item.icon}</ListItemIcon>
                            </Tooltip>
                            <Tooltip title={item.tooltip} placement={item.placement}>
                                <ListItemText primary={item.name} key={"s_nlt" + index} />
                            </Tooltip>
                        </NavLink>
                    </li>

                )}
            </List>
        </Drawer>
    );

}

export default SideBarComponent;
